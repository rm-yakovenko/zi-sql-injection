<?php

ini_set('display_errors', 1);

class DatabaseConnection
{
    private $connection;

    static private $instance;

    private function __construct()
    {
        $file = __DIR__ . '/storage.db';
        $this->connection = new PDO("sqlite:/$file");
    }

    public function execute($query)
    {
        $statement = $this->connection->prepare($query);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_OBJ);
    }

    public function insert($query, array $data = array())
    {
        $statement = $this->connection->prepare($query);
        $statement->execute($data);
    }

    static public function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }


}

class Request
{
    static public function isPost()
    {
        return !empty($_POST);
    }

    static public function get($key)
    {
        return isset($_POST[$key]) ? $_POST[$key] : false;
    }

    static public function data()
    {
        return $_POST;
    }
}

